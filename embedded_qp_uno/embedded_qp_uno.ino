#include "src/osqp/osqp.h"
#include "src/osqp/workspace.h"

void setup() {
  Serial.begin(9600);
  Serial.println(F("Setup Ok!"));
}

void loop() {
  Serial.println(F("Solving QP..."));
  osqp_solve(&workspace);

  // Print status
  Serial.print(F("Status:                "));
  Serial.println((&workspace)->info->status);
  Serial.print(F("Number of iterations:  "));
  Serial.println((int)((&workspace)->info->iter));
  Serial.print(F("Objective value:       "));
  Serial.println((&workspace)->info->obj_val);
  Serial.print(F("Primal residual:       "));
  Serial.println((&workspace)->info->pri_res);
  Serial.print(F("Dual residual:         "));
  Serial.println((&workspace)->info->dua_res);
}

---
lang: pt-BR
---

# Programação Quadrática Embarcada

## Dependências

* Python 3
 * Scipy
 * Numpy
 * [OSQP](https://osqp.org/docs/index.html) 

Instalando as dependências usando o pip:

`
pip install scipy numpy osqp
` 

## Uso

Para usar o QP de forma embarcada é necessário definir o problema de otimização
e então gerar o código embarcado. Para o solver OSQP o formato do problema pode
ser visto a seguir.

$$ 
 \min \frac{1}{2} x^T Px + q^T x \\
 s.r.\quad l \leq Ax \leq u
$$

### Mudança de coeficientes

É importante ressaltar que, no código embarcado não é possível mudar o formato
do problema, como, tamanhos de matrizes, ou, esparsidade das matrizes *A* e *P*.

Para as matrizes *P* e *A* é não é possível mudar a esparsidade, ou seja, para
uma matriz *P* da seguinte forma:

$$
P = \begin{bmatrix}
    2 & 0 & 0 & 0 \\
    0 & 2 & 0 & 0 \\
    0 & 0 & 2 & 0 \\
    0 & 0 & 0 & 2 \\
    \end{bmatrix}
$$

Não é possível mudar para a seguinte forma:

$$
P = \begin{bmatrix}
    2 & \color{red}{2} & 0 & 0 \\
    0 & 2 & 0 & 0 \\
    0 & 0 & 2 & 0 \\
    \color{red}{2} & 0 & 0 & 2 \\
    \end{bmatrix}
$$

Mas a seguinte mudança é possível:

$$
P = \begin{bmatrix}
    \color{green}{4} & 0 & 0 & 0 \\
    0 & \color{green}{4} & 0 & 0 \\
    0 & 0 & \color{green}{1} & 0 \\
    0 & 0 & 0 & \color{green}{7} \\
    \end{bmatrix}
$$

Em resumo, os coeficientes inicialmente definidos como nulos, não podem ter seu
valor modificado.

Já para as matrizes *q*, *l* e *u* é possível modificar todos os coeficientes,
já que elas não são definidas como matrizes esparsas.

### Definição das matrizes

No arquivo `generate_code.py` existem alguns exemplos demonstrando a criação de
três tipos diferentes de problemas:

* Mínimos Quadrados:
* Sistema Massa Mola:
* Solução de uma Equação Diferencial Ordinária: $(t+1) \dot{y}(t) - (t+2) y(t) = 0; \quad 0 \leq t \leq 3, \quad y(0) = 1$

### Gerando o código

Para gerar o código basta escolher o problema a ser implementado:

```python
# P, q, A, l, u = generate_mmq()
# P, q, A, l, u = generate_massa_mola()
P, q, A, l, u = generate_ode()

m.setup(P, q, A, l, u)
m.codegen('code/', project_type='Makefile',
          parameters='matrices', FLOAT=False, LONG=False)
result = m.solve()

```

E então rodar o código:

`python3 generate_code.py` 

Dessa forma será criada a pasta `code`, que possui os arquivos necessários para
embarcar o QP. O formato da pasta `code` pode ser visto a seguir:

```
├── build -> Esta pasta contém os arquivos de compilação do QP convertido para linguagem C
├── CMakeLists.txt
├── configure -> Esta pasta contém alguns arquivos de configuração do osqp
├── include -> Aqui estão os arquivos _header_ necessários para embarcar o código
└── src 
    ├── emosqp.cpython-310-x86_64-linux-gnu.so
    ├── emosqpmodule.c
    ├── example.c -> Este é um arquivo de exemplo mostrando como utilizar o código em C
    ├── osqp -> Aqui estão os arquivos _source_ necessários para embarcar o código
    └── setup.py
```

Para embarcar o solver na plataforma embarcada é necessário copiar somente o
conteúdo das pastas `include` e `src/osqp`

Como o código é gerado em C puro, é possível utilizá-lo em qualquer plataforma
que seja programável em C. A seguir serão dados alguns detalhes sobre duas
plataformas que foram testadas.

### Arduino

Para o Arduino basta colocar os arquivos das pastas `include` e `src/osqp` na
pasta `src/osqp`, localizada na raiz do projeto do arduino, da seguinte forma:

```
embedded_qp_uno
├── embedded_qp_uno.ino
└── src
    └── osqp
        ├── auxil.c
        ├── auxil.h
        ├── constants.h
        ├── error.c
        ├── error.h
        ├── example.c
        ├── examples
        ├── glob_opts.h
        ├── library.json
        ├── library.properties
        ├── lin_alg.c
        ├── lin_alg.h
        ├── osqp.c
        ├── osqp_configure.h
        ├── osqp.h
        ├── proj.c
        ├── proj.h
        ├── qdldl.c
        ├── qdldl.h
        ├── qdldl_interface.c
        ├── qdldl_interface.h
        ├── qdldl_types.h
        ├── scaling.c
        ├── scaling.h
        ├── types.h
        ├── util.c
        ├── util.h
        ├── utility
        ├── version.h
        ├── workspace.c
        └── workspace.h
```

Até o momento só foi verificado que o código compila com sucesso, mas existe uma mensagem de aviso:

```
O sketch usa 13818 bytes (42%) de espaço de armazenamento para programas. O máximo são 32256 bytes.
Variáveis globais usam 1671 bytes (81%) de memória dinâmica, deixando 377 bytes para variáveis locais. O máximo são 2048 bytes.
Pouca memória disponível, problemas de estabilidade podem ocorrer.
```

Devido à pouca memória do Arduino Uno não foi possível rodar o código. É
necessário testar em versões mais potentes, como o Due ou Mega.


### STM32F411 DISCOVERY

Para a placa Discovery, foi criado o projeto utilizando o [Cube
MX](https://www.st.com/en/development-tools/stm32cubemx.html), o arquivo de
configuração (.ioc) pode ser encontrado na pasta `embedded_qp_stm32f411`. Foi
feita somente a configuração inicial da placa de desenvolvimento e de uma porta
serial para ler o resultado da otimização.

O projeto pode ser aberto utilizando o [Cube
IDE](https://www.st.com/en/development-tools/stm32cubeide.html). Para rodar
basta clicar no botão _Run_ ou _Debug_.

![cube_ide_run_debug](./media/run_debug.png) 

A placa foi configurada para utilizar o pino `PA15` como TX da transmissão serial, 
a _baud rate_ selecionada foi de *115200*. 

A forma mais simples para testar é conectar um conversor Serial/USB e utilizar
algum leitor de serial no computador. Na imagem a seguir é possível ver a
ligação.

![connection_stm](./media/connection_stm32f411.jpg) 

Um leitor serial fácil de utilizar é o da própria [IDE do
Arduino](https://docs.arduino.cc/software/ide-v2), que já possui a opção de
plotar os dados recebidos.

Para testar rapidamente, utilize o binário compilado
`binaries/embedded_qp_stm32f411.elf` e faça a gravação do mesmo utilizando o
[Programmer](https://www.st.com/en/development-tools/stm32cubeprog.html). Após
isso, utilize a IDE do Arduino para visualizar o resultado do QP resolvendo uma
ODE (Exemplo 3). O resultado deverá ser algo do tipo:

![stm_ode](./media/emb_qp.gif) 

No gráfico é possível visualizar: o tempo necessário para resolver o QP, em
torno de $260$ ms e a resposta da ODE (a curva de $y(t)$). No serial
monitor é possível ver os números exatos, assim como os valores dos
coeficientes da série de Taylor utilizada para aproximar a função $y(t)$:

```csv
x150:2.000032,x151:1.499293,x152:0.670210,x153:0.200124,x154:0.060475,x155:0.001815,x156:0.005167,x157:-0.000707,x158:0.000145
```

No exemplo foram utilizados 9 termos para 50 amostras num período de 0 a 3
segundos.

## Otimizando o código gerado

Para gerar o código são necessárias três linhas em Python:

```python
m = osqp.OSQP()  # Cria um objeto do solver
m.setup(P, q, A, l, u)  # Configura o problema de otimização
# Gera o código
m.codegen('code/', project_type='Makefile',
          parameters='matrices', FLOAT=True, LONG=False, force_rewrite=True)
```
Argumentos:

1. *code/*: Pasta onde irá fica o código gerado
2. *project_type*: Tipo de projeto a ser compilado, também pode ser deixado vazio
   no nosso caso
3. *parameters*: Pode ser 'matrices' ou 'vectors', modifica quais tipos de
   parâmetros poderão ser modificados. Todos, inclusive as matrizes (*A* e
   *P*), ou somente os vetores (*q*, *l* e *u*)
4. *FLOAT*: Indica se é para usar `float` (True) ou `double` (False), para
   armazenar os valores
5. *LONG*: Indica se é para usar `long long` (True) ou `int` (False), para
   armazenar os índices
6. *force_rewrite*: Sobrescreve o conteúdo da pasta `code/` sempre que o código
   for executado

Uma forma de reduzir drasticamente o tamanho do código gerado é utilizar
`float` e `int` como tipos de dados, no entanto, existe um _bug_ atualmente que
faz com que os argumentos 4 e 5 não tenham efeito.
[BUG](https://github.com/osqp/osqp/issues/312).

Sem utilizar nenhum tipo de otimização, é possível carregar no STM32F411 um
problema com cerca de 129 variáveis e 120 restrições, ou seja, as seguintes
dimensões de matrizes:

$$
P \rightarrow 129 \times 129 \\
q \rightarrow 129 \times 1 \\
l \rightarrow 120 \times 1 \\
A \rightarrow 129 \times 120 \\
u \rightarrow 120 \times 1
$$

Um problema deste tamanho é aproximadamente o máximo que cabe nos $128$ KB de
memória RAM do microcontrolador.

Para aumentar um pouco a quantidade de variáveis é possível mudar o tipo `long
long` para `int` diretamente nos arquivos gerados:

```C
// Arquivo: qdldl_types.h
// QDLDL integer and float types
typedef int    QDLDL_int;   /* for indices */
typedef double  QDLDL_float; /* for numerical values  */
typedef unsigned char   QDLDL_bool;  /* for boolean values  */
```

```C
// Arquivo: osqp_configure.h
/* DLONG */
//#define DLONG
```

Com essas mudanças é possível carregar um problema de até 159 variáveis e 150
restrições, assim, ocupando em torno de $116.63$ KB ($91.12\%$) do total de
$128$ KB de memória RAM.

Em relação à memória de programa (FLASH), o total ocupado foi de $118.98$ KB
($23.24\%$) do total de $512$ KB.

OBS: O código foi compilado com otimização para velocidade `Optimize for speed
-Ofast`.

Seria possível reduzir ainda mais o tamanho do programa ao utilizar o `float`
no lugar do `double`, porém, ao fazer isso modificando os arquivos citados
acima, o _solver_ retorna um erro dizendo que o problema é não-convexo. Isto
provavelmente ocorre devido à perda de precisão dos valores. Talvez funcione
assim que o [BUG](https://github.com/osqp/osqp/issues/312) for corrigido.

## Bônus

[Conversão de MPC para QP](https://robotology.github.io/osqp-eigen/md_pages_mpc.html)


import osqp
import numpy as np
from scipy.sparse import csr_matrix


def generate_mmq():
    # q = [ 0  0  0   0   0  0  ]';
    # P = diag([0 0 2 2 2 2]);
    P = csr_matrix(np.diag([0, 0, 2, 2, 2, 2]))
    q = np.zeros((6, 1))

    # y = [ -1.0 -0.5  0.5  1.0 ];
    l = np.ones((4, 1)) * -500

    # Aeq = [ x(1)  1   -1    0    0    0
    #         x(2)  1    0   -1    0    0
    #         x(3)  1    0    0   -1    0
    #         x(4)  1    0    0    0   -1 ];
    A = csr_matrix([[0, 1, -1, 0, 0, 0],
                    [1, 1, 0, -1, 0, 0],
                    [2, 1, 0, 0, -1, 0],
                    [3, 1, 0, 0, 0, -1]])
    u = np.asarray([-1.0, -0.5, 0.5, 1.0])
    return P, q, A, l, u


def generate_massa_mola():
    """
    %% Variaveis
    % x = [x1 x2 x3]
    % onde, x3 = x2 - x1 = -x1 + x2 -x3 = 0

    %% Custo
    % min U = 0.5*k2*x1^2 + 0.5*k3*(x2 - x1)^2 + 0.5*k1*x2^2 - F*x2
    %                              |_______|
    %                                 x3
    H = diag([k2 k1 k3]);
    f = -[0 F 0]';

    %% Ax <= b
    A = [];
    b = [];

    %% Aeq x = beq
    Aeq = [-1 1 -1];
    beq = [0];

    %% Solucao
    [X, FVAL] = quadprog(H, f, A, b, Aeq, beq);
    x1 = X(1)
    x2 = X(2)
    x3 = X(3)
    U = 0.5 * X' * H * X + f' * X
    """
    k1 = 2
    k2 = 3
    k3 = 4
    F = 5

    P = csr_matrix(np.diag([k2, k1, k3]))
    q = -np.asarray([0, F, 0])

    # ~0 <= Ax <= ~0
    A = csr_matrix([
        [-1, 1, -1],
    ])

    l = np.ones((A.shape[0], 1)) * 1e-10
    u = np.ones((A.shape[0], 1)) * 1e-10
    return P, q, A, l, u


def generate_ode():
    """
    x = [E1 ... E100 | y_t1 ... y_t100 | yd_t1 ... yd_t100 | x1 ... x9]'
    ind   0 ... 99      100 ... 199        200 ... 299      300 ... 309
    """
    samples = int(50)
    variables = int(9)
    t1 = 0
    t2 = 3
    t = np.linspace(t1, t2, samples)

    P_dense = np.diag(np.concatenate((2*np.ones((samples, 1)),
                                      np.zeros((2*samples + variables, 1))),
                                     axis=None))
    P = csr_matrix(P_dense)
    q = np.zeros((3 * samples + variables, 1))

    A_dense = np.zeros((3 * samples, 3 * samples + variables))
    # E1 + (t1 + 2) y_t1 - (t1 + 1) yd_t1 = 0
    # [1 ... | (t1 + 2) ... | -(t1 + 1) ... ]
    # En + (tn + 2) y_tn - (tn + 1) yd_tn = 0
    for row in range(0, samples):
        A_dense[row, row] = 1  # En
        A_dense[row, row + samples] = t[row] + 2  # (tn + 2)
        A_dense[row, row + samples * 2] = -(t[row] + 1)  # -(tn + 1)

    # y_t1 = 1 + x1 t1^1 + x2 t1^2 + ... + x9 t1^9
    # y_t1 - x1 t1^1 - x2 t1^2 - ... - x9 t1^9 = 1
    for row in range(0, samples):
        A_dense[row + samples, row + samples] = 1  # y_t1
        for col in range(1, variables + 1):  # [1 ... m]
            A_dense[row + samples, 3 * samples +
                    col - 1] = -t[row]**col  # -tn^m

    # yd_t1 = x1 t1^0 + 2 x2 t1^1 + ... + 9 x9 t1^8
    # yd_t1 - x1 t1^0 - 2 x2 t1^1 - ... - 9 x9 t1^8 = 0
    for row in range(0, samples):
        A_dense[row + 2*samples, row + 2*samples] = 1  # yd_t1
        for col in range(1, variables + 1):  # [1 ... m]
            A_dense[row + 2*samples, 3 * samples + col - 1] = - \
                col * t[row]**(col-1)  # -m tn^(m-1)

    A = csr_matrix(A_dense)

    l = np.ones((A.shape[0], 1)) * 1e-10
    l[samples:2*samples, 0] = 1
    u = np.ones((A.shape[0], 1)) * 1e-10
    u[samples:2*samples, 0] = 1
    return P, q, A, l, u


if __name__ == "__main__":
    # P, q, A, l, u = generate_mmq()
    # P, q, A, l, u = generate_massa_mola()
    P, q, A, l, u = generate_ode()

    # min 1/2 xT P x + qT x
    #    s.t.   l <= A x <= u
    m = osqp.OSQP()
    m.setup(P, q, A, l, u)
    m.codegen('code/', project_type='Makefile',
              parameters='matrices', FLOAT=True, LONG=False,
              force_rewrite=True)
    result = m.solve()
